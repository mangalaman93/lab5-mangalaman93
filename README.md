# CSE 6230, Fall 2014, Lab 5: Tuning GPU reduction #

For instructions, see: https://bitbucket.org/gtcse6230fa14/lab5/wiki/Home

This lab is due before class on Thursday, October 9, 2014.

Experiments
===========
* see spreadsheet [here](https://docs.google.com/spreadsheets/d/18K-ex4Pgto9yE5nJWDNtSfBMa4geRKVBFDqm3PpXmjk)
* Sheet 1 represents the speedup due to successive optimization step
* Sheet 2 represents variation of performance when N is increased for various algorithms
* Sheet 3 represents variation of performance when Block Size is changed, note that the highest speedup is when BS=512
